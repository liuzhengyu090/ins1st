package com.ins1st.modules.api;

import com.ins1st.annotation.Req4Json;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/test")
public class TestController {

    /**
     * 测试接口
     * @return
     */
    @Req4Json(value = "/test",title = "测试日志",parameters = "id")
    @ApiOperation("测试接口")
    public Object test(String id){
        return "ok";
    }
}

package com.ins1st.modules.sys.mapper;

import com.ins1st.modules.sys.entity.SysBizLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 业务日志 Mapper 接口
 * </p>
 *
 * @author sun
 * @since 2019-05-10
 */
public interface SysBizLogMapper extends BaseMapper<SysBizLog> {

}

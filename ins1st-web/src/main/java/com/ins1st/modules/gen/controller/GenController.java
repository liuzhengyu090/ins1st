package com.ins1st.modules.gen.controller;

import com.ins1st.core.R;
import com.ins1st.model.GenEntity;
import com.ins1st.modules.gen.service.GenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


/**
 * 代码生成控制层
 *
 * @author sun
 */
@RequestMapping(value = "/gen")
@Controller
public class GenController {

    private static final String PREFIX = "modules/gen/";

    @Value("${spring.datasource.dynamic.datasource.master.url}")
    private String url;
    @Value("${spring.datasource.dynamic.datasource.master.username}")
    private String username;
    @Value("${spring.datasource.dynamic.datasource.master.password}")
    private String password;

    @Autowired
    private GenService genService;

    /**
     * 主页
     *
     * @return
     */
    @RequestMapping(value = "/index")
    public String index() {
        return PREFIX + "index.html";
    }


    /**
     * 生成代码
     *
     * @param genEntity
     * @return
     */
    @RequestMapping(value = "/gen")
    @ResponseBody
    public Object gen(GenEntity genEntity) {
        genEntity.setUrl(url);
        genEntity.setUsername(username);
        genEntity.setPassword(password);
        genService.gen(genEntity);
        return R.success("生成代码成功！");
    }
}

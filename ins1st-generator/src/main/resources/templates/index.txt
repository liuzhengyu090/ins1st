@layout("/common/head.html"){
<div class="layui-fluid">
    <div class="layui-row layui-col-space15">
        <div class="layui-col-md12">
            <div class="layui-card">
                <div class="layui-card-body ">
                    <form class="layui-form layui-col-space5">
                        <%for(field in map["columns"]){%>
                            <div class="layui-inline layui-show-xs-block">
                               <input class="layui-input"  autocomplete="off" placeholder="${field.comment}" name="${@com.ins1st.util.ColumnUtil.underline2Camel(field.name,true)}" id="${@com.ins1st.util.ColumnUtil.underline2Camel(field.name,true)}">
                            </div>
                        <%}%>
                        <div class="layui-inline layui-show-xs-block">
                           <button class="layui-btn"  lay-submit="" lay-filter="search"><i class="layui-icon">&#xe615;</i></button>
                        </div>
                    </form>
                </div>
                <div class="layui-card-header" id="toolBar">
                    @if(shiro.hasAuth("${map["ge"].moduleName}:${map["ge"].bizName}:add")){
                        <button class="layui-btn layui-btn" id="add">
                        <i class="layui-icon">&#xe654;</i>新增
                        </button>
                    @}
                    @if(shiro.hasAuth("${map["ge"].moduleName}:${map["ge"].bizName}:edit")){
                        <button class="layui-btn layui-btn" id="edit">
                        <i class="layui-icon">&#xe642;</i>修改
                        </button>
                    @}
                    @if(shiro.hasAuth("${map["ge"].moduleName}:${map["ge"].bizName}:del")){
                        <button class="layui-btn layui-btn-danger" id="del">
                        <i class="layui-icon">&#xe640;</i>删除
                        </button>
                    @}
                </div>
                <table class="layui-hide" id="tb" lay-filter="tb"></table>
            </div>
        </div>
    </div>
</div>
<script src="\${ctxPath}/static/modules/${map["ge"].moduleName}/${map["ge"].bizName}/${map["ge"].bizName}_index.js"></script>
@}